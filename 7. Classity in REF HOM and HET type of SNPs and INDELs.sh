#!/bin/bash

#16. Classity in REF HOM and HET type of SNPs and INDELs.
for TYPE in snp indel
do
for COLUMN in $(seq 10 58)
do 
awk -v col=${COLUMN} '!/^#/{split ($col,sample,":|,")
if (sample[1]=="./.") grade="ND"
else if (sample[4]>=30 && sample[2]/sample[4]>=0.95) grade="REF"
else if (sample[4]>=30 && sample[3]/sample[4]>=0.95) grade="HOM"
else if (sample[4]>=15 && sample[3]/sample[4]>=0.25 && sample[3]/sample[4]<=0.75) grade="HET"
else grade="NA"
print grade}' COT_cohort.${TYPE}.vcf > ${COLUMN}_COT_cohort.${TYPE}.vcf
done
#NA=Not applies
#ND=Not data
paste -d'\t' *_COT_cohort.${TYPE}.vcf > COT_cohort.${TYPE}.matrix
grep -v '#' COT_cohort.${TYPE}.vcf | cut -f1,-9 > COT_cohort.${TYPE}.rownames
head -70 COT_cohort.${TYPE}.vcf | grep '#CHROM' > COT_cohort.${TYPE}.header
#same thing for indels.
paste -d'\t' COT_cohort.${TYPE}.rownames COT_cohort.${TYPE}.matrix | \
cat COT_cohort.${TYPE}.header - > COT_cohort.${TYPE}.csv
rm COT_cohort.${TYPE}.matrix COT_cohort.${TYPE}.rownames COT_cohort.${TYPE}.header rm *${TYPE}.vcf
done
