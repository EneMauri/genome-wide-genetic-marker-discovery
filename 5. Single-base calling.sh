#!/bin/bash

#11. HaplotypeCaller
java -jar $TOOLS/GenomeAnalysisTK.jar -T HaplotypeCaller \
-R $GENOME.fa \
--emitRefConfidence BP_RESOLUTION \
-variant_index_type LINEAR \
-variant_index_parameter 128000 \
--min_base_quality_score 20 \
--min_mapping_quality_score 30 \
--dontUseSoftClippedBases \
-I ${SAMPLE}_filtered.bam \
-o ${SAMPLE}_filtered.vcf
done

#12. Cohort of variant positions
COT=$(ls COT*filtered.vcf | sed 's/^/--variant /g')
java -jar $TOOLS/GenomeAnalysisTK.jar -T GenotypeGVCFs \
-R $GENOME.fa \
-o COT_cohort.vcf $(echo $COT)

MALBEC=$(ls MALBEC*filtered.vcf | sed 's/^/--variant /g')
java -jar $TOOLS/GenomeAnalysisTK.jar -T GenotypeGVCFs \
-R $GENOME.fa \
-o MALBEC_cohort.vcf $(echo $Bene)

#13. Separates SNPs and INDELs
$TOOLS/gatk SelectVariants -V COT_cohort.vcf -O COT_cohort.indel.vcf -select-type INDEL
$TOOLS/gatk SelectVariants -V MALBEC_cohort.vcf -O MALBEC_cohort.snp.vcf -select-type SNP


