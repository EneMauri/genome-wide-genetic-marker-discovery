#!/bin/bash
TOOLS="/home/icvv/nmauri/tools/"

# 4. Adapters_marking
ls *fastq.gz |awk -F'_' '{print $1"_"$2"_"$3}' > samples.list
for SAMPLE in $(cat samples.list)
do
# 4.1. Fastq to bam file
java -jar $TOOLS/picard.jar FastqToSam \
FASTQ=${SAMPLE}_read1.fastq.gz \
FASTQ2=${SAMPLE}_read2.fastq.gz \
OUTPUT=${SAMPLE}_fasttosam.bam \
READ_GROUP_NAME=${PROJECT} \
SAMPLE_NAME=${SAMPLE} \
PLATFORM_UNIT=EneMauri PLATFORM=illumina

java -jar $TOOLS/picard.jar FastqToSam \
FASTQ=${SAMPLE}_read1.fastq.gz \
FASTQ2=${SAMPLE}_read2.fastq.gz \
OUTPUT=${SAMPLE}_fasttosam.bam

# 4.2. Marks illumina adapters with low base quality. 
java -jar $TOOLS/picard.jar MarkIlluminaAdapters \
I=${SAMPLE}_fasttosam.bam \
O=${SAMPLE}_markillumina.bam \
M=${SAMPLE}_markillumina.txt
#Plots accumulated adapter presence.
./$TOOLS/adapter_accumulated_distribution.R ${SAMPLE}_markillumina.txt
rm ${sample}_fasttosam.bam

java -jar $TOOLS/picard.jar SamToFastq \
I=${SAMPLE}_markillumina.bam FASTQ=${SAMPLE}_samtofastq.fq \
CLIPPING_ATTRIBUTE=XT CLIPPING_ACTION=2 INTERLEAVE=true NON_PF=true
rm ${SAMPLE}_markillumina.bam

