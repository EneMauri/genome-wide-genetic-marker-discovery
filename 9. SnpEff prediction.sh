#!/bin/bash

#18. SnpEff prediction 
for i in $(ls *.mq60)
do
awk '/^#/ || /PASS/' ${i} | \
$TOOLS/snpEff/snpEff.jar -v $GENOME -  > ${i}.ann
done

