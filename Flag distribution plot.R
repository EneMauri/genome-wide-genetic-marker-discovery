#### flag_distribution.R ####
#!/usr/bin/env Rscript
args <- commandArgs(TRUE)
x=read.csv(args[1], header=T, sep="\t", stringsAsFactors = F)
x=x[1:39,1:2]
#colnames(x)=c("flag","read_count")
pdf(file=paste(args[1],"pdf",sep="."))
plot(x, main=args[1])
dev.off()
