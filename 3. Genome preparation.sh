#!/bin/bash
#5.Genome indexing
#indexing for BWAMEM aligner
bwa index $GENOME
#indexing for GATK tools
samtools faidx $GENOME
#creates dictionary for GATK tools
java -jar $TOOLS/picard.jar CreateSequenceDictionary \
R=$GENOME.fa O=$GENOME.dict
