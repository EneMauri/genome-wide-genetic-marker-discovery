#!/bin/bash

#14. GATK recommended Filters
#https://gatk.broadinstitute.org/hc/en-us/articles/#360035531112--How-to-Filter-variants-either-with-VQSR-or-by-hard-filtering

#GATK Filt for SNPs
#awk '{split ($8,Fisher,"FS=|;"); split ($8,MQRankSum,"MQRankSum=|;"); split ($8,QD,"QD=|;"); split ($8,ReadPosRankSum,"ReadPosRankSum=|;"); split ($8,SOR,"SOR=| "); if (Fisher[2]<=60 && MQRankSum[2]>=-12.5 && QD[2]>=2 && ReadPosRankSum[2]>=-8 && SOR[2]<=3) print $0"\tPASS"; else print $0"\tGATKF"}' Bene_cohort.snp.csv > Bene_cohort.snp.filt.csv

awk '{split ($8,MQRankSum,"MQRankSum=|;")
split ($8,QD,"QD=|;")
split ($8,ReadPosRankSum,"ReadPosRankSum=|;")
split ($8,SOR,"SOR=| ")
if (MQRankSum[2]>=-12.5 && QD[2]>=2 && ReadPosRankSum[2]>=-8 && SOR[2]<=3) print $0"\tPASS"
else print $0"\tGATKF"}' COT_cohort.snp.csv > COT_cohort.snp.gatk.csv

#GATK Filt for INDELs
#filter "FS > 200.0"
awk '{split ($8,InbreedingCoeff,"InbreedingCoeff=|;")
split ($8,QD,"QD=|;")
split ($8,ReadPosRankSum,"ReadPosRankSum=|;")
split ($8,SOR,"SOR=| ")
if (InbreedingCoeff[2]>=-0.8 && QD[2]>=2 && ReadPosRankSum[2]>=-20 && SOR[2]<=10) print $0"\tPASS"
else print $0"\tGATKF"}' COT_cohort.indel.csv > COT_cohort.indel.gatk.csv

