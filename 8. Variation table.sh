#!/bin/bash

# 17. Counting the number of REF,HET,HOM between population.
for TYPE in snp indel
do 
awk -F'\t| ' '{print $0"\t"gsub(/REF/,"")"\t"gsub(/HET/,"")"\t"gsub(/HOM/,"")"\t"gsub(/ND/,"")"\t"gsub(/NA/,"")}' COT_cohort.${TYPE}.gatk.csv | \
awk '{if (($60+$63+$64==49)||($61+$63+$64==49)||($62+$63+$64==49)||($63+$64==49)) grade="FILT2"
else grade="SNV"
print $0"\t"grade}' > COT_cohort.${TYPE}.gatk.count.csv
done
# Number of SNV per type
awk '$59=="PASS" && $65=="SNV"' COT_cohort.snp.gatk.count.csv | wc -l
awk '$59=="PASS" && $65=="SNV"' COT_cohort.indel.gatk.count.csv | wc -l
#Number of SNVs per class HET, HOM, NA, NP, REF
awk '$59=="PASS" && $65=="SNV"{print $11}'  COT_cohort.snp.gatk.count.csv | sort -V | uniq -c
# HET clone-specific in total.
awk '($11=="REF"|| $11=="ND"||$11=="NA") && $59=="PASS" && $60>24 && $61==1 && $65=="SNV"' COT_cohort.snp.gatk.count.csv | wc -l 
awk '($11=="REF"|| $11=="ND"||$11=="NA") && $59=="PASS" && $60>45 && $61==1 && $65=="SNV"' COT_cohort.snp.gatk.count.csv | wc -l
exit
