#!/bin/bash

# Depth and coverage calculation.
for i in $(ls *.bam)
do 
$TOOLS/samtools coverage ${i} -o ${i}.cov 
$TOOLS/samtools stats ${i}> ${i}.stats
done
ls *bam | awk -F'_' '{print $2}' > rownames
for i in $(ls *.stats); do grep 'raw total' ${i} | cut -f3; done > total_reads
for i in $(ls *.cov)
do 
awk '!/#/ && !/pilon/{
reads+=$4
cov+=$6
meandepth+=$7
meanbaseq+=$8
meanmapq+=$9
}
END{
print reads"\t"cov/19"\t"meandepth/19"\t"meanbaseq/19"\t"meanmapq/19
}' ${i}
done | sed 's/\,/\./g' > cov_depth
