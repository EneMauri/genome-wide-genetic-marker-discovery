#!/bin/bash

#6. BWA-mem DEFAULT ALIGNMENT
bwa mem -M -p $GENOME ${SAMPLE}_samtofastq.fq \
-R "@RG\tID:${SAMPLE}\tSM:${SAMPLE}\tLB:${PROJECT}\tPU:EneMauri\tPL:illumina" | \
samtools view -Shb - > COT_${SAMPLE}_bwamem.bam

#7. MarkDuplicates 
samtools sort COT_${SAMPLE}_bwamem.bam -o COT_${SAMPLE}_sorted.bam
rm COT_${SAMPLE}_bwamem.bam
java -jar $TOOLS/picard.jar MarkDuplicates \
I=COT_${SAMPLE}_sorted.bam O=COT_${SAMPLE}_markdup.bam M=COT_${SAMPLE}_markdup.txt \
OPTICAL_DUPLICATE_PIXEL_DISTANCE= 2500 CREATE_INDEX=true
rm COT_${SAMPLE}_sorted.bam

#8. Realignment 
java -jar $TOOLS/GenomeAnalysisTK.jar -T RealignerTargetCreator \
-R $GENOME -I COT_${SAMPLE}_markdup.bam -o COT_${SAMPLE}_forIndelRealigner.intervals
java -jar $TOOLS/GenomeAnalysisTK.jar -T IndelRealigner \
-R $GENOME -I COT_${SAMPLE}_markdup.bam \
-targetIntervals COT_${SAMPLE}_forIndelRealigner.intervals \
-o COT_${SAMPLE}_realigned.bam
rm COT_${SAMPLE}_markdup.ba*

#9. Fix number of mismatches tag
java -jar TOOLS/picard.jar SetNmMdAndUqTags \
R=albillo_v1_scaffolds_nm.fa \
I=COT_${SAMPLE}_realigned.bam \
O=COT_${SAMPLE}_fixed.bam

#10. Filter number of mismatches tag
samtools view -h COT_${SAMPLE}_fixed.bam | \
gawk '/^@/||( ($2==99||$2==147||$2==83||$2==163) && $5==60 && /NM:i:(0|1|2|3|4|5)/)' | \
samtools view -Sbh -| samtools sort - -o COT_${SAMPLE}_filtered.bam
samtools index COT_${SAMPLE}_filtered.bam

#First results analysis (-F256 removes secondary aln)
#samtools view -F256 ${SAMPLE}_realigned.bam | awk '{print $5}' | sort -V | uniq -c | \
#awk '{print $2"\t"$1}'| cat <(echo -e mapQ"\t"read_count) - > ${SAMPLE}_realigned.mapq
#./$TOOLS/mapQ_distribution.R ${SAMPLE}_realigned.mapq
#samtools view -F256 ${SAMPLE}_realigned.bam | awk '{print $2}' | sort -V | uniq -c | \
#awk '{print $2"\t"$1}'| cat <(echo -e flag"\t"read_count) - > ${SAMPLE}_realigned.flag
#./$TOOLS/flag_distribution.R ${SAMPLE}_realigned.flag
