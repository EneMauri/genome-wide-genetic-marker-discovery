#!/bin/bash
#machine Hidalgo
#Operating System
#cat /etc/os-release
#NAME="CentOS Linux"
#VERSION="7 (Core)"
#kernel 
#uname -r
#3.10.0-957.1.3.el7.x86_64

# 1.Samples_downloading.sh
for FOLDER in \
$DATA/$USER \
$DATA/$USER \
$DATA/$USER
do 
wget -r -nH --cut-dirs=5 -nc --reject '*html*' --reject '.gif' \
--user $USER --password $PASSWORD \
$HTML/$FOLDER/
cd $FOLDER
md5sum -c md5sum.txt
cd ..
done

#2. Join technical replicates
ls $DATA/$USER/*fastq.gz > reads.list
for i in $(cat reads.list)
do 
cat $DATA/$USER/${i} $DATA/$USER/${i} $DATA/$USER/${i} > ${i}
done

#3. Initial_number_reads.sh 
for i in $(ls *fastq.gz)
do 
zcat ${i} | \
awk '/^@D00733/{getline; print}' | \
grep -cv -e '^$'| \ #remove black lines 
paste -d'\t' <(echo ${i}) - 
done > total_not_empty_reads.txt
ls *read1.fastq.gz | awk -F'_' '{print $1"_"$2"_"$3}' > samples.list
